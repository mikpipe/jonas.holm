#define POT 2

#define RX 2
#define TX 3

#include <SoftwareSerial.h>
SoftwareSerial mySerial(RX, TX);

int potVal = 0;


void setup() {
  mySerial.begin(4800);

  pinMode(POT, INPUT);
}

void loop() {
  potVal = analogRead(POT);

  mySerial.print("Hello World, ");
  mySerial.println(potVal);
  delay(potVal);
}
