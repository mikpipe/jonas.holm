// Potentiometer on 5th pin on the right
#define POT 3

// RX on 5th pin on the left
#define RX 8
// TX on 4th pin on the right
#define TX 2

#include <SoftwareSerial.h>
SoftwareSerial mySerial(RX, TX);

int potVal = 0;


void setup() {
  mySerial.begin(4800);

  pinMode(POT, INPUT);
}

void loop() {
  potVal = analogRead(POT);

  mySerial.print("Hello World, ");
  mySerial.println(potVal);
  delay(potVal);
}
