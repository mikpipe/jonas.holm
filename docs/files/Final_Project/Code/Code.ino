#include <SoftwareSerial.h>

// rx / tx pin
const byte rxPin = 6;
const byte txPin = 5;

SoftwareSerial mySerial(txPin, rxPin);

// Microphone pin
const int micPin = A4;

// LEDs pins
const int led1Pin = 1;
const int led2Pin = 2;
const int led3Pin = 3;

int db;
int micVal;
int dbCorrectionValue = -10;


void setup() {
  mySerial.begin(4800);

  // Set LED-Pins as outputs
  pinMode(led1Pin, OUTPUT);
  pinMode(led2Pin, OUTPUT);
  pinMode(led3Pin, OUTPUT);
}

void loop() {
  micVal = analogRead(micPin);
  
  db = (micVal + 83.2073) / 11.003 + dbCorrectionValue;
  mySerial.println(micVal);
  mySerial.println(db);

  // Low risk
  if (db < 70) {
    mySerial.println("State 1");
    digitalWrite(led1Pin, HIGH);
    digitalWrite(led2Pin, LOW);
    digitalWrite(led3Pin, LOW);
  }
  // Medium risk
  else if (db >= 70 && db < 90) {
    mySerial.println("State 2");
    digitalWrite(led1Pin, LOW);
    digitalWrite(led2Pin, HIGH);
    digitalWrite(led3Pin, LOW);
  }
  // High risk
  else if (db >= 90) {
    mySerial.println("State 3");
    digitalWrite(led1Pin, LOW);
    digitalWrite(led2Pin, LOW);
    digitalWrite(led3Pin, HIGH);
  }

  delay(100);
}
