# Final Project Planning

## Necessary electronic parts

### Input
- Microphone

### Output
- 3 LEDs (Red, green, blue)

### Additional things
- Battery


## PCB

### Microphone 
- 5V
- GND
- Input pin

### For each LED
- GND
- Outputpin

### Battery
- 5V
- GND

### Total requirements
- 2x 5V
- 5x GND
- 3 output pins
- 1 input pin


## Steps
1. Designing the PCB
2. Produce the PCB
3. Designing the case
4. Produce the case
5. Programming