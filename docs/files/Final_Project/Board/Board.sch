EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 4050 4350 0    50   Input ~ 0
+5V
Text GLabel 9250 3350 0    50   Input ~ 0
+5V
Text GLabel 9250 3550 0    50   Input ~ 0
+5V
Text GLabel 9250 3450 0    50   Input ~ 0
GND
Text GLabel 9250 3650 0    50   Input ~ 0
GND
Text GLabel 7650 4550 0    50   Input ~ 0
GND
Text GLabel 7650 4650 0    50   Input ~ 0
GND
Text GLabel 7650 4750 0    50   Input ~ 0
GND
Wire Wire Line
	7650 4750 7900 4750
Wire Wire Line
	7650 4650 7900 4650
Wire Wire Line
	7650 4550 7900 4550
Wire Wire Line
	9250 3650 9500 3650
Wire Wire Line
	9250 3550 9500 3550
Wire Wire Line
	9250 3450 9500 3450
Wire Wire Line
	9250 3350 9500 3350
$Comp
L FAB:Power_+5V #PWR01
U 1 1 61B3801A
P 5650 3250
F 0 "#PWR01" H 5650 3100 50  0001 C CNN
F 1 "Power_+5V" H 5665 3423 50  0000 C CNN
F 2 "" H 5650 3250 50  0001 C CNN
F 3 "" H 5650 3250 50  0001 C CNN
	1    5650 3250
	1    0    0    -1  
$EndComp
$Comp
L FAB:Power_GND #PWR02
U 1 1 61B3885A
P 5650 5100
F 0 "#PWR02" H 5650 4850 50  0001 C CNN
F 1 "Power_GND" H 5655 4927 50  0000 C CNN
F 2 "" H 5650 5100 50  0001 C CNN
F 3 "" H 5650 5100 50  0001 C CNN
	1    5650 5100
	1    0    0    -1  
$EndComp
$Comp
L FAB:Microcontroller_ATtiny44A-SSU U1
U 1 1 61B34ED3
P 5650 4250
F 0 "U1" H 5650 5131 50  0000 C CNN
F 1 "Microcontroller_ATtiny44A-SSU" H 4500 4800 50  0000 C CNN
F 2 "FAB:SOIC-14_3.9x8.7mm_P1.27mm" H 5650 4250 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc8183.pdf" H 5650 4250 50  0001 C CNN
	1    5650 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5650 3250 5650 3400
Wire Wire Line
	5650 4950 5650 5100
$Comp
L FAB:C C1
U 1 1 61B3E773
P 5650 2650
F 0 "C1" V 5902 2650 50  0000 C CNN
F 1 "1uF" V 5811 2650 50  0000 C CNN
F 2 "FAB:C_1206" H 5688 2500 50  0001 C CNN
F 3 "" H 5650 2650 50  0001 C CNN
	1    5650 2650
	0    -1   -1   0   
$EndComp
$Comp
L FAB:R R1
U 1 1 61B3FA93
P 4500 4350
F 0 "R1" V 4707 4350 50  0000 C CNN
F 1 "10K" V 4616 4350 50  0000 C CNN
F 2 "FAB:R_1206" V 4430 4350 50  0001 C CNN
F 3 "~" H 4500 4350 50  0001 C CNN
	1    4500 4350
	0    -1   -1   0   
$EndComp
Text GLabel 7700 3000 0    50   Input ~ 0
RST
Text GLabel 7700 3100 0    50   Input ~ 0
PA6
Text GLabel 7700 3300 0    50   Input ~ 0
PA5
Text GLabel 7700 3400 0    50   Input ~ 0
PA4
Wire Wire Line
	7700 3300 7900 3300
Wire Wire Line
	7700 3400 7900 3400
Wire Wire Line
	7700 3100 7900 3100
Wire Wire Line
	7700 3000 7900 3000
Wire Wire Line
	5650 3400 6400 3400
Wire Wire Line
	6400 3400 6400 3850
Wire Wire Line
	6400 3850 6250 3850
Connection ~ 5650 3400
Wire Wire Line
	5650 3400 5650 3550
Text GLabel 4650 4600 0    50   Input ~ 0
RST
Wire Wire Line
	4050 4350 4350 4350
Wire Wire Line
	4650 4350 4850 4350
Wire Wire Line
	4650 4600 4850 4600
Wire Wire Line
	4850 4600 4850 4350
Connection ~ 4850 4350
Wire Wire Line
	4850 4350 5050 4350
Text GLabel 5300 2650 0    50   Input ~ 0
GND
Text GLabel 6000 2650 2    50   Input ~ 0
+5V
Wire Wire Line
	6000 2650 5800 2650
Wire Wire Line
	5500 2650 5300 2650
Text GLabel 7700 3900 0    50   Input ~ 0
PA3
Text GLabel 7700 4000 0    50   Input ~ 0
PA2
Wire Wire Line
	7700 3900 7900 3900
Wire Wire Line
	7900 4000 7700 4000
Text Label 7550 3700 0    50   ~ 0
LED_Out_Mic_In
Text Label 8000 4400 2    50   ~ 0
LED_GND
Text GLabel 4950 4150 0    50   Input ~ 0
PB1
Wire Wire Line
	4950 4150 5050 4150
Wire Wire Line
	7750 3800 7900 3800
Text GLabel 7700 4100 0    50   Input ~ 0
PA1
Wire Wire Line
	7700 4100 7900 4100
Text Label 7350 2800 0    50   ~ 0
Connectors_Board_Arduino
Text GLabel 6250 4450 2    50   Input ~ 0
PA6
Text GLabel 6250 4350 2    50   Input ~ 0
PA5
Text GLabel 6250 4250 2    50   Input ~ 0
PA4
Text GLabel 6250 4150 2    50   Input ~ 0
PA3
Text GLabel 6250 4050 2    50   Input ~ 0
PA2
Text GLabel 6250 3950 2    50   Input ~ 0
PA1
$Comp
L Connector_Generic:Conn_01x03 J4
U 1 1 61BA84A8
P 8100 4650
F 0 "J4" H 8180 4692 50  0000 L CNN
F 1 "Conn_01x03" H 8180 4601 50  0000 L CNN
F 2 "eagle_pin:03P" H 8100 4650 50  0001 C CNN
F 3 "~" H 8100 4650 50  0001 C CNN
	1    8100 4650
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J5
U 1 1 61BAA330
P 9700 3450
F 0 "J5" H 9780 3442 50  0000 L CNN
F 1 "Conn_01x04" H 9780 3351 50  0000 L CNN
F 2 "eagle_pin:04P" H 9700 3450 50  0001 C CNN
F 3 "~" H 9700 3450 50  0001 C CNN
	1    9700 3450
	1    0    0    -1  
$EndComp
Text Label 9300 3200 0    50   ~ 0
Power
$Comp
L Connector_Generic:Conn_01x04 J3
U 1 1 61BAECCF
P 8100 3900
F 0 "J3" H 8180 3892 50  0000 L CNN
F 1 "Conn_01x04" H 8180 3801 50  0000 L CNN
F 2 "eagle_pin:04P" H 8100 3900 50  0001 C CNN
F 3 "~" H 8100 3900 50  0001 C CNN
	1    8100 3900
	1    0    0    -1  
$EndComp
Connection ~ 7900 3800
Wire Wire Line
	7900 3800 7950 3800
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 61BBA17E
P 8100 3000
F 0 "J1" H 8180 2992 50  0000 L CNN
F 1 "Conn_01x02" H 8180 2901 50  0000 L CNN
F 2 "eagle_pin:02P" H 8100 3000 50  0001 C CNN
F 3 "~" H 8100 3000 50  0001 C CNN
	1    8100 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 61BBAA65
P 8100 3300
F 0 "J2" H 8180 3292 50  0000 L CNN
F 1 "Conn_01x02" H 8180 3201 50  0000 L CNN
F 2 "eagle_pin:02P" H 8100 3300 50  0001 C CNN
F 3 "~" H 8100 3300 50  0001 C CNN
	1    8100 3300
	1    0    0    -1  
$EndComp
Text GLabel 9200 4150 0    50   Input ~ 0
+5V
Text GLabel 9200 4250 0    50   Input ~ 0
GND
Wire Wire Line
	9200 4150 9450 4150
Wire Wire Line
	9200 4250 9450 4250
Text Label 9050 4050 0    50   ~ 0
9V_5V_Converter
$Comp
L Connector_Generic:Conn_01x03 J6
U 1 1 61BD2202
P 9650 4250
F 0 "J6" H 9730 4292 50  0000 L CNN
F 1 "Conn_01x03" H 9730 4201 50  0000 L CNN
F 2 "eagle_pin:03P" H 9650 4250 50  0001 C CNN
F 3 "~" H 9650 4250 50  0001 C CNN
	1    9650 4250
	1    0    0    -1  
$EndComp
Text GLabel 9200 4350 0    50   Input ~ 0
V_In
Text GLabel 9200 4550 0    50   Input ~ 0
V_In
Text GLabel 9200 4650 0    50   Input ~ 0
GND
Wire Wire Line
	9450 4350 9200 4350
Wire Wire Line
	9200 4550 9450 4550
Wire Wire Line
	9200 4650 9450 4650
$Comp
L Connector_Generic:Conn_01x02 J7
U 1 1 61BD3C18
P 9650 4550
F 0 "J7" H 9730 4542 50  0000 L CNN
F 1 "Conn_01x02" H 9730 4451 50  0000 L CNN
F 2 "eagle_pin:02P" H 9650 4550 50  0001 C CNN
F 3 "~" H 9650 4550 50  0001 C CNN
	1    9650 4550
	1    0    0    -1  
$EndComp
Text GLabel 7750 3800 0    50   Input ~ 0
PB1
$EndSCHEMATC
