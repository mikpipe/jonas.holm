EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L FAB:Microcontroller_ATtiny44A-SSU IC1
U 1 1 61926CFD
P 5500 3900
F 0 "IC1" H 4450 4500 50  0000 C CNN
F 1 "Microcontroller_ATtiny44A-SSU" H 4400 4400 50  0000 C CNN
F 2 "FAB:SOIC-14_3.9x8.7mm_P1.27mm" H 5500 3900 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc8183.pdf" H 5500 3900 50  0001 C CNN
	1    5500 3900
	1    0    0    -1  
$EndComp
$Comp
L FAB:C C1
U 1 1 6192928C
P 5500 2400
F 0 "C1" H 5615 2446 50  0000 L CNN
F 1 "1uF" H 5615 2355 50  0000 L CNN
F 2 "FAB:C_1206" H 5538 2250 50  0001 C CNN
F 3 "" H 5500 2400 50  0001 C CNN
	1    5500 2400
	0    -1   -1   0   
$EndComp
$Comp
L FAB:R R1
U 1 1 6192AEFE
P 4150 4000
F 0 "R1" H 4220 4046 50  0000 L CNN
F 1 "10K" H 4220 3955 50  0000 L CNN
F 2 "FAB:R_1206" V 4080 4000 50  0001 C CNN
F 3 "~" H 4150 4000 50  0001 C CNN
	1    4150 4000
	0    -1   -1   0   
$EndComp
$Comp
L FAB:BUTTON_B3SN SW1
U 1 1 6192C22A
P 6900 4200
F 0 "SW1" H 6900 4485 50  0000 C CNN
F 1 "BUTTON_B3SN" H 6900 4394 50  0000 C CNN
F 2 "FAB:Button_Omron_B3SN_6x6mm" H 6900 4400 50  0001 C CNN
F 3 "https://omronfs.omron.com/en_US/ecb/products/pdf/en-b3sn.pdf" H 6900 4400 50  0001 C CNN
	1    6900 4200
	1    0    0    -1  
$EndComp
$Comp
L FAB:Power_GND #PWR0101
U 1 1 619300C4
P 5500 4800
F 0 "#PWR0101" H 5500 4550 50  0001 C CNN
F 1 "Power_GND" H 5505 4627 50  0000 C CNN
F 2 "" H 5500 4800 50  0001 C CNN
F 3 "" H 5500 4800 50  0001 C CNN
	1    5500 4800
	1    0    0    -1  
$EndComp
$Comp
L FAB:LED LED1
U 1 1 61930F1C
P 6700 3500
F 0 "LED1" H 6693 3716 50  0000 C CNN
F 1 "Red" H 6693 3625 50  0000 C CNN
F 2 "FAB:LED_1206" H 6700 3500 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 6700 3500 50  0001 C CNN
	1    6700 3500
	-1   0    0    1   
$EndComp
$Comp
L FAB:Power_+5V #PWR0102
U 1 1 6192F1D4
P 5500 2950
F 0 "#PWR0102" H 5500 2800 50  0001 C CNN
F 1 "Power_+5V" H 5515 3123 50  0000 C CNN
F 2 "" H 5500 2950 50  0001 C CNN
F 3 "" H 5500 2950 50  0001 C CNN
	1    5500 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 2950 5500 3000
Wire Wire Line
	5500 4600 5500 4800
Wire Wire Line
	5500 3000 6200 3000
Wire Wire Line
	6200 3000 6200 3500
Wire Wire Line
	6200 3500 6100 3500
Connection ~ 5500 3000
Wire Wire Line
	5500 3000 5500 3200
Text GLabel 7500 4200 2    50   Input ~ 0
GND
Wire Wire Line
	7100 4200 7500 4200
Text GLabel 4450 4500 0    50   Input ~ 0
RST
Wire Wire Line
	4600 4500 4600 4000
Wire Wire Line
	4600 4000 4900 4000
Wire Wire Line
	4300 4000 4600 4000
Connection ~ 4600 4000
$Comp
L FAB:R R2
U 1 1 619478A4
P 7150 3500
F 0 "R2" V 7357 3500 50  0000 C CNN
F 1 "499" V 7266 3500 50  0000 C CNN
F 2 "FAB:R_1206" V 7080 3500 50  0001 C CNN
F 3 "~" H 7150 3500 50  0001 C CNN
	1    7150 3500
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6850 3500 7000 3500
Wire Wire Line
	7250 2600 6900 2600
Wire Wire Line
	7250 2750 6900 2750
Wire Wire Line
	7250 2850 6900 2850
Wire Wire Line
	7250 2950 6900 2950
Wire Wire Line
	6550 3500 6300 3500
Wire Wire Line
	6300 3500 6300 3600
Wire Wire Line
	6300 3600 6100 3600
Wire Wire Line
	6100 3800 6300 3800
Wire Wire Line
	6100 3900 6300 3900
Wire Wire Line
	6100 4000 6300 4000
Wire Wire Line
	6100 4100 6300 4100
Text GLabel 6300 3800 2    50   Input ~ 0
PA3
Text GLabel 6300 3900 2    50   Input ~ 0
PA4
Text GLabel 6300 4000 2    50   Input ~ 0
PA5
Text GLabel 6300 4100 2    50   Input ~ 0
PA6
Wire Wire Line
	6100 4200 6700 4200
Text GLabel 6900 2950 0    50   Input ~ 0
PA3
Text GLabel 6900 2850 0    50   Input ~ 0
PA4
Text GLabel 6900 2750 0    50   Input ~ 0
PA5
Text GLabel 6900 2600 0    50   Input ~ 0
PA6
Text GLabel 7500 3500 2    50   Input ~ 0
GND
Wire Wire Line
	7300 3500 7500 3500
Wire Wire Line
	4450 4500 4600 4500
Text GLabel 3700 4000 0    50   Input ~ 0
+5V
Wire Wire Line
	3700 4000 4000 4000
Text GLabel 5000 2400 0    50   Input ~ 0
GND
Text GLabel 6000 2400 2    50   Input ~ 0
+5V
Wire Wire Line
	5000 2400 5350 2400
Wire Wire Line
	5650 2400 6000 2400
$Comp
L Connector_Generic:Conn_01x04 Power1
U 1 1 6194C3E0
P 6900 4800
F 0 "Power1" H 6980 4792 50  0000 L CNN
F 1 "Conn_01x04" H 6980 4701 50  0000 L CNN
F 2 "eagle_pin:04P" H 6900 4800 50  0001 C CNN
F 3 "~" H 6900 4800 50  0001 C CNN
	1    6900 4800
	-1   0    0    1   
$EndComp
Text GLabel 7450 4800 2    50   Input ~ 0
GND
Text GLabel 7450 4900 2    50   Input ~ 0
+5V
Wire Wire Line
	7100 4900 7450 4900
Wire Wire Line
	7100 4800 7450 4800
Text GLabel 7450 4600 2    50   Input ~ 0
GND
Text GLabel 7450 4700 2    50   Input ~ 0
+5V
Wire Wire Line
	7450 4600 7100 4600
Wire Wire Line
	7450 4700 7100 4700
Text GLabel 6900 2500 0    50   Input ~ 0
RST
Wire Wire Line
	6900 2500 7250 2500
Text GLabel 6300 3700 2    50   Input ~ 0
PA2
Wire Wire Line
	6100 3700 6300 3700
Text GLabel 4800 3800 0    50   Input ~ 0
XTAL2
Wire Wire Line
	4800 3800 4900 3800
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 61961E9F
P 8550 4750
F 0 "J2" H 8630 4742 50  0000 L CNN
F 1 "Conn_01x02" H 8630 4651 50  0000 L CNN
F 2 "eagle_pin:02P" H 8550 4750 50  0001 C CNN
F 3 "~" H 8550 4750 50  0001 C CNN
	1    8550 4750
	1    0    0    -1  
$EndComp
Text GLabel 8350 4750 0    50   Input ~ 0
PA2
Text GLabel 8350 4850 0    50   Input ~ 0
XTAL2
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 6196CA09
P 7450 2850
F 0 "J3" H 7530 2892 50  0000 L CNN
F 1 "Conn_01x03" H 7530 2801 50  0000 L CNN
F 2 "eagle_pin:03P" H 7450 2850 50  0001 C CNN
F 3 "~" H 7450 2850 50  0001 C CNN
	1    7450 2850
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J1
U 1 1 6196D6A1
P 7450 2500
F 0 "J1" H 7530 2492 50  0000 L CNN
F 1 "Conn_01x02" H 7530 2401 50  0000 L CNN
F 2 "eagle_pin:02P" H 7450 2500 50  0001 C CNN
F 3 "~" H 7450 2500 50  0001 C CNN
	1    7450 2500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
