// File which includes the frequencies for the different notes
#include "pitches.h"

#define SPEAKER 3

#include <SoftwareSerial.h>
const byte RX = 9;
const byte TX = 2;
SoftwareSerial mySerial(RX, TX);

// Array with the melody
int melody[] = {
    NOTE_C5,
    NOTE_D5,
    NOTE_E5,
    NOTE_F5,
    NOTE_G5,
    NOTE_A5,
    NOTE_B5,
    NOTE_C6};

// Array which includes the length of each note
// Duration: 8 = quarter note, 4 = 8th note, 2 = 16th note, ...
int noteDurations[] = {
    8,
    8,
    8,
    8,
    8,
    8,
    8,
    8};

// BPM (beats per minute)
int bpm = 90;

void setup()
{
  pinMode(SPEAKER, OUTPUT);

  mySerial.begin(4800);
}

void loop()
{
  play();

  delay(3000);
}

void play()
{
  for (int noteIndex = 0; noteIndex < sizeof(melody); noteIndex++)
  {
    int noteDuration = bpm * noteDurations[noteIndex];
    tone(SPEAKER, melody[noteIndex], noteDuration * .95);

    mySerial.println(melody[noteIndex]);

    delay(noteDuration);

    noTone(SPEAKER);
  }
}
