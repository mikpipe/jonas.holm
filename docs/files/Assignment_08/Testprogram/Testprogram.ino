#define LED 13
#define BUTTON 8

#include <SoftwareSerial.h>
SoftwareSerial mySerial(10, 11);

boolean buttonState;

byte cnt;
byte iterations;


void setup() {
  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);
  
  Serial.begin(9600);
  mySerial.begin(4800);

  buttonState = false;
  
  cnt = 0;
  iterations = 5;
}


void loop() {
  mySerial.println("Hello world!");
  delay(1000);
  
  /* buttonState = digitalRead(BUTTON);
  digitalWrite(LED, buttonState); */
  
  
  /* if (cnt < iterations) {
    blink();
    cnt++;
  } */
}

void blink() {
  digitalWrite(LED, HIGH);
  delay(500);
  digitalWrite(LED, LOW);
  delay(500);
}
