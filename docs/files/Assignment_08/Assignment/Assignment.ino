#define LED 1
#define BUTTON 7

#include <SoftwareSerial.h>

const byte rxPin = 9;
const byte txPin = 2;

SoftwareSerial mySerial(rxPin, txPin);

boolean buttonState = false;

int code[] = {0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0};


void setup()
{
  // Data rate for SoftwareSerial port
  mySerial.begin(4800);

  pinMode(LED, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);
}

void loop()
{
  buttonState = digitalRead(BUTTON);

  delay(500);

  if (!buttonState)
  {
    for (int i = 0; i < sizeof(code); i++)
    {
      if (code[i] == 0)
      {
        mySerial.println("short");
      }
      else
      {
        mySerial.println("long");
      }

      blink(250 + code[i] * 250);
    }
  }
}

void blink(int time)
{
  digitalWrite(LED, HIGH);
  delay(time);
  digitalWrite(LED, LOW);
  delay(time);
}
