# 01. Markdown & Project Management

This week I started to work with Markdown and Git to get started with my website.

## Markdown

Markdown is an easy way to format text (and also added images) in an easy, uncomplicated way.

### Useful links

- [Markdown](https://en.wikipedia.org/wiki/Markdown)

### Code Examples

Some of basic markdown commands are:

- **Headlines**
  
  Add '#' before the text of the headline, e.g. '# Headline'

  Number of '#' depends on what heading level you want to use
- **Bold & Italic**
  
  Bold: '**' before and after the word(s) that shall appear bold
  
  Italic: '*' before and after the word(s) that shall appear italic
- **Lists**
  
    
    Add '1.' for ordered and '-' for unordered lists ('1. First element', '- First element')


## Git

Git is a platform that can be used by developers for version control of their projects. Furthermore, it offers the possibility to track issues while developing and to use CI/CD pipelines.

### Commands

In the following, I will explain some of git's basic commands that are required for its usage.

- **Enter user name:**
  
  git config --global user.name "Example user"

- **Enter user email:**
  
  git config --global user.email user@example.com

- **Create a local copy of a repository to work in it:**
  
  git clone username@host:path/of/repository

- **Add a specific file to staging:**
  
  git add "name of file"

- **Commiting all added files:**
  
  git commit -m "message"

- **List the files that have been changed or that still need to be added or committed:**
  
  git status

- **List all branches and display the branch you're currently working in:**
  
  git branch

- **Changes that have been made in the remote repository are pulled to your local directory:**

  git pull

- **All branches or one specific branch is pushed to the remote repository:**
  
  git push origin "branchname"