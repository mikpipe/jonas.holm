# 08. Embedded programming
This week, I programmed my board with my own code. When you press the button on my board, the LED output will be my name in morse code.

## How I made my code
### Preparation
First, I defined some variables which are required to run my program. So I defined the pins of the Attiny chip which the LED and the button are connected to. In my case, the LED was connected to pin 1 and the button was connected to pin 7.

<pre><code>
    #define LED 1
    #define BUTTON 7
</code></pre>

Furthermore, I have set up the Software Serial in- and output which offers the oppertunity to receive or send date for testing purposes. First assigning the RX-pin to pin 9 of the attiny chip and the TX-pin to pin 2 of the ATtiny chip. The number of the pins can be taken from the [datasheet](https://wkla.no-ip.biz/ArduinoWiki/lib/exe/fetch.php?w=800&tok=385f7d&media=arduino:arduinosps:attiny84_core.jpeg) of the ATtiny 84. Then, I initialized the Software Serial and used the rxPin and txPin variables as parameters.


<pre><code>
    const byte rxPin = 9;
    const byte txPin = 2;

    SoftwareSerial mySerial(rxPin, txPin);
</code></pre>

Next, I created a boolean called "buttonState" which contains the currant state of the button. "True" means that the button is currently pressed while the boolean is "False" as long as the button is not pressed. As the derfault state is that the button is not pressed, I have set the boolean to 'false' in the beginning.

<pre><code>
    boolean buttonState = false;
</code></pre>

As the last preperation step, I initialized an integer array which includes the morse codes for all single letters of my name in a row. '0' stands for 'short' while '1' stands for 'long'.


### Setup-Function
The setup function is called once at the beginning of the program. As the name already tells, it's role is to setup everything necessary, for example to initialize variables with their start values.

<pre><code>
    mySerial.begin(4800);

    pinMode(LED, OUTPUT);
    pinMode(BUTTON, INPUT_PULLUP);
    
    buttonState = false;
</code></pre>

In my setup function, I first set the data rate for the software serial port. It is measured in bps (bytes per second) and can have a maximum value of 115200bps. For my personal use, I set it to 4800 bps.

Forthermore, I set the pin mode of the LED to "OUTPUT" and the pin mode of the button to "INPUT". By doing this, the two components are recognized as input and output devices and can be used as such.


### Loop-Function
The loop function is called after the setup function and is looped infinitely. Inside the loop function, the main program code is written.

The first thing that is done inside the loop function is to read the current state of the board's button and to assign the value to the boolean 'buttonState'.

<pre><code>
    buttonState = digitalRead(BUTTON);
</code></pre>

Next, an if-statement checks whether the button is currently pressed or not. If the button is pressed, a for-loop will start. Inside the round brackets of the loop, the integer 'i' will be set to 0. As long as 'i' is smaller than the size of the array with the morse code, the loop will run again and 'i' will be increased by 1 each time.
The code inside the for-loop controls whether the board's LED will be blinking short or long. To make this possible, I wrote a seperate function which is called "blink".

<pre><code>
    void blink(int time)
    {
        digitalWrite(LED, HIGH);
        delay(time);
        digitalWrite(LED, LOW);
        delay(time);
    }
</code></pre>

Inside the "blink"-function, the command 'digitalWrite' is used to either set the LED's status to "HIGH" (= the LED is turned on) or "LOW" (= the LED is turned off).
In between, a delay is inserted so that the LED is turned on for a certain period of time. As you can see, the length of the delay is dependant of the parameter "time" which is required when calling this function.

<pre><code>
    blink(250 + code[i] * 250);
</code></pre>

As you can see, the blink time for the short morse signal is 250ms. Additionally, the number in the current array cell (code[i]) is multiplied with 250 and added to the 250ms. This results ininking time of 250 + 0 * 250 = 250ms for short signals and 250 + 1 * 250 = 500ms for long signals.

Furthermore, I am using an "if... else"-statement inside the for-loop to print the current signal length to the serial output.

<pre><code>
    if (code[i] == 0)
    {
        mySerial.println("short");
    }
    else
    {
        mySerial.println("long");
    }
</pre></code>

If the current number is a 0, "short" is printed to the serial monitor inside the Arduino IDE. Else, the output is "long". Like that, it is yet a bit easier to keep track of what's happening while the program is running. Turthermore, the serial output can be used in more complex programs where it's helping even more to keep track of everything that's going on.

### Upload the code

Before you upload your code, it's important that the board is connected to the Arduino. Furthermore, the ground pin of the cable for reading the serial data has to be connected to the second GND pin of your board while the RX and TX pin have to be connected to the pins you declared in your code.

![serialcable](../images/week08/Serial_Cable_JPEG.jpg "serialcable")

The other side of the cable has to be plugged into one of your USB ports. When everything is connected, you can upload your program to the board via 'Sketch --> Upload using programmer'. 


## GIF of the code working with the board

![boardblinking](../images/week08/Board_blinking_GIF.gif "boardblinking")

![serialoutput](../images/week08/Serial_Output.png "serialoutput")

## Download link of the program code
Download my code [here](../files/Assignment_08/Assignment/Assignment.ino).