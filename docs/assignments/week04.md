# 04. Electronics production

This week, I cutted, soldered and programmed a small electronic board with an LED that is blinking every second.

The "hello_board"-folder with all required files can be downloaded [here](../files/Assignment_04_Hello_Board.zip).

## How to engrave the PCB

First, you need to engrave the PCB. This requires some preparation as the files for the cutting machine have to be created.

### Preparation

First, you need to open the file "hello.kicad_pcb" from the "hello_board"-folder inside KiCad. Then, create an SVG-file out of this file by clicking on File --> Export --> SVG and use these settings:

![kicadexportoptions](../images/week04/KiCAD_SVG_Export_Options.png "kicadexportoptions")


Your exported SVG-file should look like this:

![exportedsvg](../images/week04/SVG_exported.png "exportedsvg")

The next step is to convert the SVG-file to a .png-file. To do that, it's neccassary to import the SVG-file into Inkscape and to export it as a PNG.

For cutting, you need three seperate files to tell the machine which parts shall be cutted with which settings. This is done by using grayscale images which contain black and white parts. All the black parts will be cutted and all white parts remain untouched by the machine.
To create such grayscale images, you can use Gimp, import the PNG image and color the neccessary parts in black or white.
The first grayscale image is for the traces. So the traces (and also the holes as they will ne cut out later) have to be colored completely white while the rest has to be black.
Secondly, for the holes everything except the holes themselves has to be black.
And lastly, you need to create a third file for the cutout. As the board's edges shall be rounded, you have to use Gimp's selection tool to select the entire image as a square and then make the square's corners rounded by going to Selection --> Rounded corners. The inner area of the rounded square has to be white and the outside black.

All three images then have to be exported as PNGs again.

As a last step for preparation, you have to convert the PNG files into files that the cutting machine can read properly. The website https://fabmodules.org can be used for this convertion process.

First, select "Input format" --> Image (.png) and upoad your file. Then, set the output format to Roland mill (.rml). For the image with the traces, select "PCB traces (1/64)", for the files with the holes and the cutout, select "PCB outline (1/32)". Then, make sure that the parameters are the same as the ones on the following images (first image: parameters for cutout; second image: parameters for traces):

![settingstraces](../images/week04/Settings_cutout.png "settingstraces")
![settingscutout](../images/week04/Settings_traces.png "settingscutout")

Lastly, click on "Calculate", wait some seconds and then save the file on your computer. Now, you now can use the three files to cut out your PCB.

### Engraving

For engraving, you first need to place a plate you want to use inside the machine. It's important to turn on the vacuum to ensure that it's not moving during the cutting process.

![plateback](../images/week04/Plate_back.jpg "plateback")
![boarvacuum](../images/week04/Board_on_Vacuum.jpg "boarvacuum")

Furthermore, it's important to use the right head for the machine, depending on whether you want to make the holes / cutout (0.4mm) or you want to do the traces (0.8mm). Remember to change it when going from cutout to the traces or the other way around.

The next step is so set the origin point of the machine. This should always be at the bottom left of the plate. First, position the cutter on the x- and y-axis. Then, turn on the cutter so that it's rotating and slowly move it down on the z-axis. You can use the different existing speeds to be more precise. As soon as the cutter slightly scratches the board, click on "Set XYZ origin here " --> Apply

![cuttersettings](../images/week04/Cutter_Settings.jpg "cuttersettings")

Then, move up the cutter a little bit so that it's not touching the board anymore.

Now, click on "Cut", select the files you want to cut and start the cutting process. When everything is completely cutted, you can use the vacuum cleaner to remove the dust particles, turn of the vacuum which fixes the material and then remove the finished board from the machine.

## How to solder the board

When it comes to soldering, it also requires a few steps of preperation. First, open the file "hello.pro" inside KiCad and then doubleclick on "hello.kicad_pcb" from the sidebar on the left
Then, you need to export a material sheet which includes all the electronic parts that need to be soldered on the board. This sheet can be exported with File --> Fabrication Outputs --> BOM File. Afterwards, it can be opened the exported file in Excel and take the materials that are written in the Excel Sheet from the cabinet in the FabLab.

Then, it comes to the soldering process. First, place each one of the single parts on your board. Then, use the solder wire and the soldering iron to solder each part onto the board. When you are done with soldering the partsa onto the board, chech whether the electronic works by using the potentiometer.

## How to program the board

The last step is to program the board. To do that, you first need to prepare the board.

First, you need to open the preferences dialog inside the Arduino IDE and enter the URL https://raw.githubusercontent.com/damellis/attiny/ide-1.6.x-boards-manager/package_damellis_attiny_index.json into the field "Additional Board Manager URLs". Next, make sure that nothing is connected to your Arduino, connect it with your PC and select the right port and board inside the IDE. Then, go to File --> Example and open the "Arduino as ISP"-sketch. This sketch has to be uploaded to the Arduino.

Now, it's time to connect the board to the Arduino. Disconnect the Aruino from the PC and connect the board to the Arduino. It's important to make sure that all the connections are correct.

![arduinoconnection](../images/week04/Arduino_Connection.png "arduinoconnection")

Lastly, reconnect Arduino to the PC and set the right settings inside the IDE:

- Board: attiny25/45/85
- Processor: attiny45
- Frequency internal 8 MHZ

Then, go to Tools --> Arduino as ISP as programmer.

Before you complete the preparation, check all parameters and then go to Tools --> Burn Bootloader to complete the process.

Now, you can write own program and upload it to the board by clicking "Sketch --> Upload using programmer".

The file with my code for making the LED blink can be downloaded [here](../files/Assignment_04_blink_hello_board_fun.ino).

In the end, the final board with the blinking LED looks like this:

![ledblinking](../images/week04/LED_blinking.gif "ledblinking")
