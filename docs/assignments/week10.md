# 10. Output devices

This week's topic was 'Output devices'. I used programmed my board so that it can play a simple, short melody on a speaker.


## How I made my code

Before I started with the actual coding, I created a seperate file called "pitches.h". In this file, I defined all notes with their frequency. You can import this file into the arduino code the following way:

<pre><code>
    #include "pitches.h"
</pre></code>

The advantage of defining all the notes in a seperate file is that they don't take away space in your actual code file.

After importing the pitches file, I first assigned the speaker to pin 3 and set up the Software Serial.

<pre><code>
    #define SPEAKER 3

    #include <SoftwareSerial.h>
    const byte RX = 9;
    const byte TX = 2;
    SoftwareSerial mySerial(RX, TX);
</pre></code>

Next, I defined two arrays; one for the notes of the melody and one for the duration of the notes. Furthermore, I set an integer for the melody's bpm.

<pre><code>
    int melody[] = {
        NOTE_C5,
        NOTE_D5,
        NOTE_E5,
        NOTE_F5,
        NOTE_G5,
        NOTE_A5,
        NOTE_B5,
        NOTE_C6
    };


    int noteDurations[] = {
        8,
        8,
        8,
        8,
        8,
        8,
        8,
        8
    };

    int bpm = 90;
</pre></code>

In the setup function, I set up the Speaker as an output device and intialized the serial port with a data rate of 4800.

<pre><code>
    void loop()
    {
    play();

    delay(3000);
    }
</pre></code>

I wrote an extra function called "play" which plays my melody. The function's for-loop iterates through the "melody" array and plays one note after another. Furthermore, the length of each note from the "noteDurations" array is considered as well. Additionally, I am logging the frequency of the current note to the serial monitor.

<pre><code>
    void play()
    {
        for (int noteIndex = 0; noteIndex < sizeof(melody); noteIndex++)
        {
            int noteDuration = bpm * noteDurations[noteIndex];
            tone(SPEAKER, melody[noteIndex], noteDuration * .95);

            mySerial.println(melody[noteIndex]);

            delay(noteDuration);

            noTone(SPEAKER);
        }
    }
</pre></code>

"play" is called from the loop function. After the melody has been played once, a delay of three seconds is inserted. Afterwards, the melody is played again. This procedure is repeated over and over again.

## Connecting the output device
For using the speaker, I first had to solder two cables to it. The speaker's left cable is connected to a pin of the board, in my case to pin 10 of my ATtiny chip, while the right cable goes to the gnd pin of the board itself or of the Arduino. It doesn't matter which of the two gnd pins you choose. Then, you just need to upload the code to your board to run it.

## Video showing the board working with output device

I had some problems with including a video player that plays a video of my board, but the video can be downloaded [here](../images/week10/Video.mov).


## Photos

!["board01"](../images/week10/IMG_6246.jpg)

!["board02"](../images/week10/IMG_6247.jpg)

!["board03"](../images/week10/IMG_6248.jpg)


## Download link of my program code

Download my code [here](../files/Assignment_10/Assignment/Assignment.ino) and the additional "pitches"-file [here](../files/Assignment_10/Assignment/pitches.h).