# 03. Computer controlled cutting

## How to make a design for the laser cutter
To make a design which can be cut with the laser cutter, you can use a software like Fusion 360.
In Fusion 360, I made different sketches for the front & the back, the left & right sides and the top & bottom parts of my clock. I made the design parametric which has the big advantage that I can for example change the width or the depth of the joints without having to change everything manually.

- ### Front & Back
  ![design_front](../images/week03/Design_Front.png "design_front")
![design_back](../images/week03/Design_Back.png "design_back")

- ### Top & Bottom
  ![design_top_bottom](../images/week03/Design_Top_Bottom.png "design_top_bottom")

- ### Sides
  ![design_sides](../images/week03/Design_Sides.png "design_sides")


When the design itself is finished, you have to export it as a .dfx-file so that it can be opened by the software for the lasercutter. It's best to export the file immediately to an USB stick or to copy it onto a stick after the export is done.

## How to use the laser cutter
### The software
When the preparation is done, you can open your exported design on the PC which has the software for the lasercutter installed.

The first thing that needs to be done is to create different layers. You can give a layer different colors. Later, you will have the possibility to set different options for your layers based on their colors such as the speed of the laser and it's power.
It's important that all lines that shall be cutted with the same strength belong to the same layer. For example, if you want to cut a rectangle which has text engraved on it, the text should be one layer and the rectangle should be another, seperate layer as for engraving, the laser may not cut as strong as for cutting out the rectangle.

Then, it is recommended to place all pieces that will be cutted out close together. This is useful because that way there is less free space between the parts and less material is wasted.
![picture01](../images/week03/Assignment_03_01.jpg "picture01")

The next step in the preparation is to position the elements in the printing area. You can open the window for that by pressing Ctrl + P and then selecting "Set Properties". By positioning the elements inside the printing area, you tell the program where the lasercutter shall start to cut the elements out of the material.
![picture02](../images/week03/Assignment_03_02.jpg "picture02")

The last preparation step inside the software is the color mapping. (click on "Properties") You can set the speed, the power, the frequency and the focus for each color that you previously used to color the layers of your design. If you for example set parameters for Red, every element that is part of a layer which is colored red will be cut with the set parameters. In my case, I only had one layer as I had to cut out the single shapes as well as small circles inside the front panel of my clock for the LEDs.
The frequency is the amount of times the laser "hits" the material in one second to cut it. It's recommended to use a value around 4000Hz here. For the speed and the power, it's recommended to have one of these two values fixed (e.g. the power fixed to 100) and to change the other one depending on what material you are working with and whether you want to cut or to engrave something. It's important to consider that certain materials like cardboard are cut more easily than others.
After having set all parameters for one color, you have to press the symbol with the the arrows (the one between the + and the -) to apply the changes you've made. When you are all done, you can click on "OK" and then on "Print".
![picture03](../images/week03/Assignment_03_03.jpg "picture03")

### The lasercutter itself
After you clicked "Print" inside the software, your file is send to the lasercutter. But before you start cutting your design, it is neccessary to set the starting point and the focus of the cutter. The starting point is the top left corner of the cuter's viewport while the focus is the distance from the laser to the material you want to cut. As a help to set the focus, you can use the metal pin which can be flipped down. If its hanging straight down and barely touches the material,the focus is set correctly.
When everythign is set correctly, the laser cutter's cover can be closed and the cutting process can be started by pressing the green "Confirm"-button.



## Picture(s) of the final object
![clockfront](../images/week03/Clock_front.jpg "clockfront")
![clocktop](../images/week03/Clock_top.jpg "clocktop")

## Download links:
  - [Design](../files/Assignment_03_Clock_parametric.f3d)