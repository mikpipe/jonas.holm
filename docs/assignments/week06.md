# 06. 3D printing

This week, I designed and 3d printed a simple smartphone stand.

## The 3D design

I made my 3d design by first making the basic shape of the stand. Then, I created a new sketch to first sketch and then extrude various connectors for the stand so that it is stable and doesn't break immediately when you place a smartphone on it.

The overall dimensions of my 3d model are:

- width: 60mm
- length: 95mm
- height: 130mm

First, I had some issues with figuring out how exactly I will print my model, because there are always some parts where it is necessary to print additional support parts for stablelize cerain parts which otherwise would float in the air. So I added some additional connecters to make printing easier.

Additionally, I made the single parts of the stand a bit thicker. With the width I chose first, the entire model would have been too instable and probably could not have carried a smartphone properly.

My finished 3d model with the proper dimensions and the additional connectors looks like this:
![3ddesign](../images/week06/3D_Design_final.png "3ddesign")

When the model is finished, you have to export is as an .stl-file. The exort option can be found under File --> Export. Just enter a matching name, choose the correct location and select "STL FIles (\*.stl)" as the file type. Then click "Export" to finish.

## Settings for slicing

Next, you need to create a .3mf-file that can be read by the printer and which includes all the settings for slicing. For that, we use the software Cura.

### Add printer

When Cura opens up, you first need to set up a printer. When you use Cura for the first time, you see a menu where you can add a printer, but alternatively you can also open the printer menu and click on "Add printer".

![addprinter](../images/week06/Cura_add_Printer.png "addprinter")

In the next step, the following window opens up:
![addprintermenu](../images/week06/Cura_add_Printer_Menu.png "addprintermenu")

Click on "Add a non-networked printer" and select the right printer which you want to use. In my case, I used "Ultimaker 2+".

### Import model & printing setup

After setting up a printer, you can import your model and set it up for printing.

The model can be imported by going to File --> Open File(s). After importing, it will appear in the viewport. You now can move or also rotate your model if necessary. If your model for example has one big, flat surface, it is recommended to rotate the model so that the big surface is facing down.

Next, the settins for 3d printing have to be set up. The settings menu can be opens by clicking on the menu entry on the top ight of the software.

![settingsmenu](../images/week06/Cura_Settings_Menu.png "settingsmenu")

The settings which you enter depend on the model you want to print. For example, for more detailled models the layer height should be lower than for models which have for example big, flat surfaces. Furthermore, you should pay attention that you don't set the printing speed to high. Otherwise, this could lead to problems while printing as the printing head is not able to be as precise as necessary.

Lastly, it's also important that you add additonal support pieces when needed. You can determine whether your model requires support pieces by rotating it. If there are any visible red surfaces, this means that this surfaces cannot be printed without additional support.

![printsettingssupport](../images/week06/Cura_Print_Settings_Support.png "printsettingssupport")

This are the settings that I used for printing my 3d model:

![printsettings](../images/week06/Cura_Print_Settings.png "printsettings")

Now as everything is set up, the model needs to be exported as a g-Code-file (File --> Export). The Ultimaker S2+ printers in the fablab don't have USB ports, so you have to save the file to an SD card.

## How to use the 3D printer

### Preparations

Before you start printing, the printer has to be turned on by switching the switch on the back to "On". Then, insert the SD card into the slot on the printer.

Furthermore you should make sure that there is still enough material available so that the printer doesn't run out of material during the printing process. Cura shows you in the bottom right corner how much material your printed model will take. In case there is not enough material left in the printer, you can easily change it and insert a new role. Simply select "Material" and then eject the current material. It will take a moment as the head heats up first. As soon as the material has been ejected from the printer, you can remove the old role, mount a new one, inject the beginning of the material into the printer and cofirm the material change on the printer's screen.

### Printing

When the setup of the printer is completed, you can browse the files on the SD card and select your model to print it. After selecting, the printer will first warm up the head and the build plate and then automatially starts printing. On the screen, you can see the time that still remains.

![printing01](../images/week06/Printing_01.jpg "printing01")
![printing02](../images/week06/Printing_02.jpg "printing02")

## Removing the printed model from the printer

When your model is finished, you can carefully pull it off the build plate. If you used support pieces, you can carefully remove them by breaking them off the model.

This is how my finished model looks like:

![modelfinished](../images/week06/Model_finished.jpg "modelfinished")

## Download link of the 3D design

The 3D design I printed can be downloaded [here](../files/Assignment_06_Smartphone_Stand_Fusion.f3d).