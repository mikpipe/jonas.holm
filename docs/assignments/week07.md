# 07. Electronics Design

This week, I created a custom board with and LED and an additional button on it, so that you're able to turn the LED on and off.

## How did you design your board?

I designed my board using KiCad, just as I already did before in week 04.

First, I made a scheme with all the needed parts on it. In the end, it looked like this:

![board01](../images/week07/Design_KiCad_01.png "board01")

In the next step, you have to assign the board's PCB footprints which means that you tell KiCad which excact parts are gonna be used for buiding the board later.

The settings menu for setting the PCB footprints can be opened by clicking ont the PCB footprint button in the headbar. It's the symbol on the right next to the bug.

![pcbfootprintsbutton](../images/week07/Assign_PCB_Footprints_Button.png "pcbfootprintsbutton")

On the left, there is a list with various different libraries. On the top, you can choose between various filters so that KiCad just displays you elements which fit to the part of the board that is currently selected in the list in the middle.

![pcbfootprintsfilters](../images/week07/PCB_Footprints_Filters.png "pcbfootprintsfilters")

For this board, I just used parts from the FAB-library. The only exception were the pin connectors which are from the eagle_pin-library.

You can assign a footprint to a part of your board by selecting the part you want to assign the footprint to in the list in the middle. Then, navigate to the library of your choice and select a fitting part. When you use the filter options of KiCad, this process is really easy as the programm automatically shows you which available footprints are compatible.

![pcbfootprints](../images/week07/PCB_Footprints.png "pcbfootprints")

Afer assigning all the footprints, I generated the board layout out of my scheme by clicking the green button far on the right in the headbar at the top of the program window.

![generatelayoutbutton](../images/week07/Generate_Board_Layout_Button.png "generatelayoutbutton")

When clicking the button, a new window opens. Here, you see all the elements which you previosly created. They are probably placed a bit chaotic first, so it's best to rearrange them. KiCad shows you which elements will be connected by traces later on the finished board, so it is highly recommended to arrange all the elements in a way that no traces are crossing.

![board02](../images/week07/Design_KiCad_02.png "board02")

When you finished your design, you can export it as an SVG file (File --> Export --> SVG). Then, you have to import the SVG file into Inkscape and export it as a PNG image.

In the next step, the single images for the cutout, the holes and the traces have to be created. You can do that by using Gimp or any other equivalent image editing software (Photoshop, ...). For a detaild description on how to make these three images, go to [my report of week 04](week04.md).

As the last preperation step, the three image files have to be converted into files with fitting settings that the machine can actually read. This can be done by using the website fabmodules.org. In the top left, you can import the single image files. next, set the output format to "Roland Mill" and select "PCB outlines" for the cutout and the holes and "PCB traces" for the traces. Lastly, select "MX-40" as the machine you want to use and make sure that the settings on the right fit to these settings:

### Cutout:

![cutoutsettings](../images/week07/Cutout_Settings.png "cutoutsettings")

### Holes

![holessettings](../images/week07/Holes_Settings.png "holessettings")

### Traces

![tracessettings](../images/week07/Traces_Settings.png "tracessettings")

### CNC mill the PCB

As I already explained all details for CNC milling in my ocumentation for week 04, I won't repeat all the single steps very detailed here.
After I placed a plate inside the machine and fixed it with two stripes of tape because the vacuume was broken, I inserted the 0.4mm cutting head into the machine to cut the traces.

Then, I set the machine's origin point and started the cutting process. Afterwards, I switched to the 0.8mm cutting head and repeated the cutting process to cut out the board itself and to create the holes.

![cutting](../images/week07/Cutting.png "cutting")

### Solder the components

For collecting the components I had to solder onto the board, I exported a list with all needed components by opening the board view and clicking on File --> Fabrication Outputs --> BOM File. Then, I went through the list and took all the necessary parts from the cabinet in the FabLab.

Then, I soldered all the components one after another onto my PCB. Here, it was important to pay attention that they are all placed correctly and that for example the chip is orientated correctly and that the LED is rotated the right way so that the plus- and minus-Sides are placed on the correct traces.

Lastly, I checked whether the electricity flow is correct everywhere on the board. After this step was also completed, I moved on to the last step: the programming.

## Program the board with a blink

First, I had to connect the Arduino I was using to my Laptop without the PCB connected to it to upload the "Arduino ISP"-sketch. **Important**: Make sure that under "Tools" you select the Arduino Uno as the used board. Otherwise, it won't work and you'll receive an error.

Then, I plugged in all the necessary cables into the PCB and the Arduino. The exact connections can be found inside the datasheet of the Attiny84 chip.

Next, I burned the bootloader to the PCB. Now, you need to change the board under "Tools" to "Attiny 24/44/84" as you now are addressing the chip on the PCB, not the Arduino.

Lastly, I wrote a short program which lets the LED of the board blink once every second and uploaded it by using the "Upload as programmer" option under "Sketch".

<pre><code>
  void setup() {
    pinMode(1, OUTPUT);
  }
  
  void loop() {
    digitalWrite(1, HIGH);
    delay(1000);
    digitalWrite(1, LOW);
    delay(1000);
  }
</code></pre>

### Picture of the board blinking

This is how my final board looks like:

![boardfinal](../images/week07/Board_final.png "boardfinal")

![boardblinkinggif](../images/week07/Board_final_blinking_GIF.gif "boardblinkinggif")

## Download link of the board design files

The KiCad files for my board design can be downloaded [here](../files/Assignment_07/Board.zip).
