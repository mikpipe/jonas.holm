# 09. Input devices

THis week, I used a potentiometer in combination with my board to use an input signal for changing the serial output depending on the current input value.

## How I made my code
### Preperation
In my code, I first declard the input for my potentiometer. I chose the right pin of my board's 3 pin connector, which goes to pin 3 of my Attiny chip.

<pre><code>
    #define POT 3
</pre></code>

Next, I defined the RX and TX pins and set up the Software Serial. The RX pin is pin 8 (the upper pin of my board's 2-pin-connector) and the TX pin is pin 2 (the bottom one of the 2-pin-connector).

<pre><code>
    #define RX 8
    #define TX 2

    #include <SoftwareSerial.h>
    SoftwareSerial mySerial(RX, TX);
</pre></code>

Lastly, I declared an integer for storing the current value of the potentiometer and set it to 0.

<pre><code>
    int potVal = 0;
</pre></code>

### Setup
In the setup function, I set the rate of the software serial to 4800. Furthermore, I have set the potentiometer as an input.

<pre><code>
    void setup() {
        mySerial.begin(4800);
        
        pinMode(POT, INPUT);
    }
</pre></code>

### Loop
My actual program is quite simple. I print "Hello World" together with the current value of the potentiometer to the serial output and add a delay afterwards. The length of the delay depends on the current value of the potentiometer, so it can be between 0ms and 1023ms.

<pre><code>
    void loop() {
        potVal = analogRead(POT);

        mySerial.print("Hello World, ");
        mySerial.println(potVal);
        delay(potVal);
    }
</pre></code>


## How I connected the sensor

The potentiometer has three pins which can be used to connect it to the board. The left pin is for GND, the middle one is for connecting the potentiometer to the board to change electricity flow and the right one is meant for the 5V connection. Sometimes, it can be a bit tricky to keep the cables attached to the potentiometer's pins. So you either could use a small piece of tape to fix the cables directly on the potentiometer, or you could put the potentiometer onto a breadboard to ensure a stable connection.

![potentiometer](../images/week09/Potentiometer.png "potentiometer")

![board](../images/week09/Board.png "board")

![boardpotentiometer](../images/week09/Board_Potentiometer.png "boardpotentiometer")

After the potentiometer has been connected to the board, you need to connect the FTDI cable to the board. (more detailed description for that in [last week's assignment](week08.md))

![ftdicable](../images/week09/FTDI_Cable.png "ftdicable")

## Video showing the board working with the sensor

![boardgif](../images/week09/Board_GIF.gif "boardgif")

![serialoutputscreenshot](../images/week09/Serial_Output_Screenshot.png "serialoutputscreenshot")

## Download link
My code of this week can be found [here](../files/Assignment_09/Potentiometer/Potentiometer.ino).