# 02. Computer aided design

My idea for the final prject is to make a simple clock that has LEDs on it's front which display the current time.

## 2D design
I made the 2D sketch of my clock by using LibreCAD.
LibreCAD offers various possibilities to create simple lines. For example, you can create a line by choosing two points on the grid or by selecting the "Vertical" or "Horizontal" option which lets you create a line on the seelcted axis.

In my case, I used the "Horizontal"- and "Vertical"-feature under the "Line"-option as my clock will have a simple, rectangular shape which requires only horizontal and vertical, but no diagonal lines.

The red dots on the front are the LEDs which will later display the hours and minutes.

The clock has a width of 200mm and a length height of 60mm. LibreCAD also has a feature which gives you the possibility to display the dimensions next to your sketch.

![librecaddimensions](../images/week02/LibreCAD_Dimensions.png "librecaddimensions")

Just as when creating the lines, there are different options you can choose from to create arrows which show the dimensions of your sketch. For adding the dimensions for the width and height, I again chose the "Vertical" and "Horizontal" option and then made the lines as long as neccessary. By doing a left-click, you then confirm the lenght and the arrows are displayed next to your sketch.

![sketch2d](../images/week02/Sketch_2D.png "sketch2d")


## 3D design
For the 3D sketch, I used Fusion 360 by Autodesk.

The clock will be made out of six single parts. The front and the back as well as the top and the bottom part all have the same dimensions of 200mm x 60mm x 5mm while the left and right sides will be 60mm x 60mm x 5mm.

In Fusion 360, I made the single parts of the clock by creating cubes for each part. You can do this by clicking on Volumetric body --> Create --> Cube. After doing that, you will habe to select an axis on which the ground of the cube will be. Then, you can enter the lenght, width and height of the cube and confirm everything by clicking "OK" in the popup menu on the right.

The LEDs are currently represented by small cylinders which are working as placeholders. They can be created the same way as Cubes:
- Click on "Volumetric body --> Create --> Cylinder"
- Enter dimensions
- Confirm with "OK"

Inside the clock, there will be all the electronic parts such as batteries, cables for the LEDs and the control unit.

![sketch3d](../images/week02/Sketch_3D.png "sketch3d")


### Download links:
- 2D design file:
    - [2d design](../files/Assignment_02_Project_2D.dxf)
- 3D design file
    - [3d design](../files/Assignment_02_Project_3D.f3d)