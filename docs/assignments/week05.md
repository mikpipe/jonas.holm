# 05. CNC milling

This week I made my own 3d model inspired by the triforce symbol from the videogame series "The Legend of Zelda" by using CNC milling.

## How I made the 3D model

To create the 3d model I used for cutitng, I first made a sketch in Fusion 360.
I created three triangles which together shape the triforce.
Furthermore, I added the word "Zelda" on the right side. First, I planned to write "The Legend of Zelda" but it turned out that there was not enough space and the text was too small to be cut out precisely.
In the next step, I extruded the text as well as the triangles. The height of the text is 5mm, the height of the triangles is 15mm. Then, I used the chamfer tool of Fusion 360 to change the triangles to pyramids as I wanted to habe 3d and not just 2d objects.
At the end, it's important to combine all existing bodys to one object so that everything can be cutted correctly.

## Generating the GCode and CAM settings

### Switching to manufacture view

The next step is to make the file ready for cutting. To do that, you need to switch from the design view to the manufacture view by using the drop down menu in the top left corner.

![manufactureselection](../images/week05/Manufacture_Selection.png "manufactureselection")

### Setup of the model

First, you need to setup the zero point of your 3d model. Click on Setup --> New Setup on the top left and set the zero point of your model to it's highest point at the bottom left under the option "Stock Point". Confirm by clicking "Okay".

![setup](../images/week05/Setup.png "toolsadaptive01")

### Used cutting methods & tool selection

For one model, you can use use different cutting methods (2D or 3D). That way, you can go from a first rough cut to a more detailed cut.

To cut out your model, different sized tools can be used. The tool menu can be opened by first choosing a cutting method and then clicking on "Select" under "Tool" in the Tool-Tab of the cutting method.
![toolselection](../images/week05/Tool_Selection.png "toolselection")

Now, you can either create new tools by clicking on the "+"-symbol or you can choose an existing one.

![toolcreation](../images/week05/Tool_Creation.png "toolcreation")

For my model, I used cutting the following cutting methods with the following settings:

1. Adaptive Clearing with 6mm flat end mill to cut away wider areas
   ![toolsadaptive01](../images/week05/Tools_Adaptive_01.png "toolsadaptive01")

2. Adaptive Clearing with 3mm flat end mill to cut away smaller areas, e.g. in between the single letters
   ![toolsadaptive02](../images/week05/Tools_Adaptive_02.png "toolsadaptive02")

3. 2D contour with 3mm flat end mill for cutting out the small areas inside the "e", the "d" and the "a"
   ![tools2d](../images/week05/Tools_2D.png "tools2d")

4. Parallel with 3mm flat end mill for making the pyramids smoother and reducing the "steps"
   ![toolsparallel](../images/week05/Tools_Parallel.png "toolsparallel")

A setting you have to set for all cutting methods is the so called "Machine boundary" which basically is the minimum height to which the machine shall cut. You can change this setting under the Geometry Tab by setting "Machining Boundary" to "Selection", then clicking on "Machining Boundary Selection" and select the edges that are your model's boundaries.

![boundaryselection](../images/week05/Boundary_Selection.png "boundaryselection")

### Simulated cutting inside Fusion 360

When you are done with setting up everything, you can use the simulation feature under "Action" --> "Simulate" to check whether everything is working properly.

### Exporting the model

The last step is to export your model with it's settings into a file the cutting machine can read. Here, it's important to knwo that all cutting-methods that use the same cutting head with the same size can be exported into one file. In my case, this means, that I can export the adaptive clearing with the 3mm cutting head, the 2D contour method and the parallel method together as I used a 3mm cutting head for all of them.
To do that, you need to select all cutting methods you want to export, right-click and then choose "Post process". In the window that opens up, you need to enter the following values:

![exportpostprocess](../images/week05/Export_Postprocess.png "exportpostprocess")

Then, click on "Post" and save the file to a USB stick.

## Cutting

For cutting, you first need to place the piece of material you want to cut inside the machine. As it can be difficult to fix the material with the vacuume (depends onwhich material you use), it can be helpful to use a bit of double sided duct tape instead.
Then, you need to insert the right cutting head into the machine and set the XYZ-origin of the cutter. This is already familiar from the past weeks which is why I won't repeat it here again.
As a last step, you go to "Cut", select all the files you want to cut (which actually just should just be one as everything that used the same head can be combined into just one file) and then start the cutting process. After that, change the cutting head and repeat the process with the file(s) that is/are still left to do.

## Final product

This is how my final model looked like in the end:

![finalmodel](../images/week05/Model_finished.jpg "finalmodel")

The project file of my 3d model I used for milling can be downloaded [here](../files/Assignment_05_CNC.f3d).
